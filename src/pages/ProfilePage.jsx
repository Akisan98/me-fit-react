import AppContainer from "../hoc/AppContainer";
import {withRouter} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {createAccount} from "../store/actions/accountActions";
import {useState} from "react";
import LoadingComponent from "../components/UserFeedback/LoadingComponent";
import './ProfilePage.css';
import Button from '@mui/material/Button';
import * as React from "react";


const ProfilePage = () => {
    const {admin, firstName, lastName, height, weight, email, contributor, imageUrl} = useSelector(state => state.auth)
    const [inputFirstname, setFirstname] = useState(firstName);
    const [inputLastname, setLastname] = useState(lastName);
    const [inputHeight, setHeight] = useState(height);
    const [inputWeight, setWeight] = useState(weight);
    const [inputEmail, setEmail] = useState(email);
    const [contributorStatus, setContributorStatus] = useState(contributor);
    const [inProcess, setInProcess] = useState(false);

    const dispatch = useDispatch()

    function handleFirstnameChange(event) {
        setFirstname(event.target.value);
        console.log(event.target.value);
    }

    function handleLastnameChange(event) {
        setLastname(event.target.value);
        console.log(event.target.value);
    }

    function handleEmailChange(event) {
        setEmail(event.target.value);
        console.log(event.target.value);
    }

    function handleHeightChange(event) {
        setHeight(event.target.value);
        console.log(event.target.value);
    }

    function handleWeightChange(event) {
        setWeight(event.target.value);
        console.log(event.target.value);
    }

    function handleContributorChange() {
        setContributorStatus(2)
        console.log("send request clicked ");
    }

    const handleCreation = (event) => {
        setInProcess(false)
        console.log("submit clicked");
        event.preventDefault();
        dispatch(createAccount({
            inputFirstname,
            inputLastname,
            inputHeight,
            inputWeight,
            inputEmail,
            contributorStatus
        }))
        console.log("Profile page state " + inputFirstname + " " +
            inputLastname + " " +
            inputHeight + " " +
            inputWeight + " " +
            inputEmail + " " +
            contributorStatus)
    }

    function handleEditClicked() {
        setInProcess(true)
        console.log("edit clicked");
    }

    return (
        <AppContainer>
            <div className="pageContainer">
                <LoadingComponent/>
                <h1>Profile</h1>
                <br/>
                <div className="profileHeader">
                    <img className="profileItem" src={imageUrl} alt="" style={{maxWidth: "5rem"}}/>
                    <div>
                        <p style={{margin: 0, fontWeight: 700, fontSize: "2rem"}}>{firstName} {lastName}</p>
                        {admin === 3 ?
                            <p style={{fontWeight: 500, fontSize: "1rem"}}>Role: Admin</p>
                            : contributor === 3 ?
                                <p style={{fontWeight: 500, fontSize: "1rem"}}>Role: Contributor</p>
                                : contributor === 2 ?
                                    <p style={{fontWeight: 500, fontSize: "1rem"}}>Role: Pending Request</p>
                                    :
                                    <p style={{fontWeight: 500, fontSize: "1rem"}}>Role: User</p>
                        }
                    </div>

                </div>
                <br/>
                {inProcess ?
                    <>
                        <br/>
                        <form onSubmit={handleCreation}>
                            <div className="profileHeader">
                                <div className="field">
                                    <label>Firstname&nbsp;&nbsp;&nbsp;&nbsp; </label>
                                    <input className="fancyInput" type="text" placeholder={firstName} onChange={handleFirstnameChange}/>
                                </div>
                                <div>
                                    <label>Lastname&nbsp;&nbsp;&nbsp;&nbsp; </label>
                                    <input className="fancyInput" type="text" placeholder={lastName} onChange={handleLastnameChange}/>
                                </div>
                            </div>
                            <br/>

                            <div className="field">
                                <label>Email&nbsp;&nbsp;&nbsp;&nbsp; </label>
                                <input style={{width: 530}} className="fancyInput" type="text" placeholder={email} onChange={handleEmailChange}/>
                            </div>
                            <br/>

                            <div className="profileHeader">
                                <div className="field">
                                    <label> Height&nbsp;&nbsp;&nbsp;&nbsp; </label>
                                    <input className="fancyInput" type="number" placeholder={height} onChange={handleHeightChange}/>
                                </div>
                                <div>
                                    <label>Weight&nbsp;&nbsp;&nbsp;&nbsp; </label>
                                    <input className="fancyInput" type="number" placeholder={weight} onChange={handleWeightChange}/>
                                </div>
                            </div>
                            <br/>
                            {(contributor === 1 || contributor === 0) &&

                            <label>
                                <h6>Request contributor access?</h6>
                                <input className="checkbox" type="checkbox" onChange={handleContributorChange}/>
                                Yes, please!
                            </label>
                            }
                            <br/>
                            <br/>
                            <br/>
                            <Button sx={{mr: 2}} variant="contained" type="submit" onClick={handleEditClicked}>Save</Button>
                            <Button variant="contained" type="cancel" color="error" onClick={handleEditClicked}>Cancel</Button>
                        </form>
                    </>
                    :
                    <>
                        <br/>
                        <form onSubmit={handleCreation}>
                            <div className="profileHeader">
                                <div className="field">
                                    <label>Firstname&nbsp;&nbsp;&nbsp;&nbsp; </label>
                                    <input disabled={true} className="fancyInputDisable" type="text" placeholder={firstName} onChange={handleFirstnameChange}/>
                                </div>
                                <div>
                                    <label>Lastname&nbsp;&nbsp;&nbsp;&nbsp; </label>
                                    <input  disabled={true} className="fancyInputDisable" type="text" placeholder={lastName} onChange={handleLastnameChange}/>
                                </div>
                            </div>
                            <br/>

                            <div className="field">
                                <label>Email&nbsp;&nbsp;&nbsp;&nbsp; </label>
                                <input  disabled={true} style={{width: 530}} className="fancyInputDisable" type="text" placeholder={email} onChange={handleEmailChange}/>
                            </div>
                            <br/>

                            <div className="profileHeader">
                                <div className="field">
                                    <label> Height&nbsp;&nbsp;&nbsp;&nbsp; </label>
                                    <input  disabled={true} className="fancyInputDisable" type="number" placeholder={height} onChange={handleHeightChange}/>
                                </div>
                                <div>
                                    <label>Weight&nbsp;&nbsp;&nbsp;&nbsp; </label>
                                    <input disabled={true}  className="fancyInputDisable" type="number" placeholder={weight} onChange={handleWeightChange}/>
                                </div>
                            </div>
                            <br/>
                            <br/>
                            <Button disabled={inProcess}  variant="contained" onClick={handleEditClicked}>Edit</Button>
                        </form>
                    </>
                }
            </div>
        </AppContainer>
    );
};

export default withRouter((ProfilePage));


