import AppContainer from "../hoc/AppContainer";
import * as React from 'react';
import './AdminPage.css';
import {useSelector} from "react-redux";
import AdminTable from "../components/Admin/AdminTable";
import LoadingComponent from "../components/UserFeedback/LoadingComponent";

const AdminPage = () => {

    const {contributors, requests} = useSelector(state => state.admin)

    return (
        <AppContainer>
            <LoadingComponent/>
            <h2 style={{color: 'white'}}>Requests</h2>
            <AdminTable list={requests} isContributorList={false}/>
            <br/>
            <h2 style={{color: 'white'}}>Contributors</h2>
            <AdminTable list={contributors} isContributorList={true}/>
        </AppContainer>
    );
};

export default AdminPage;