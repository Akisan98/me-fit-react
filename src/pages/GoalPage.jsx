import AppContainer from "../hoc/AppContainer";
import Progress from "../components/Progress/Progress";
import WorkoutList from "../components/WorkoutList/WorkoutList";
import CompletedGoalList from "../components/CompletedGoalList/CompletedGoalList";
import ProgramList from "../components/ProgramList/ProgramList";
import LoadingComponent from "../components/UserFeedback/LoadingComponent";
import {useDispatch, useSelector} from "react-redux";
import {goalActionCompleted} from "../store/actions/goalActions";
import React, {useState} from "react";
import Button from "@mui/material/Button";
import Modal from "@mui/material/Modal";
import Box from "@mui/material/Box";
import {useHistory} from "react-router-dom";
import Confetti from "../components/Confetti";

const style = {
    borderRadius: 3,
    color: 'black',
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 450,
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 4,
};

const GoalPage = () => {
    console.log('Goals.render');

    const {currentGoal, goalCompleted, completedGoals, hasGoal} = useSelector(state => state.goals)
    const dispatch = useDispatch();

    const history = useHistory();

    const handleRouteGoal = () =>{
        dispatch(goalActionCompleted(currentGoal))
        let path = 'dashboard';
        history.push(path);
    }

    const [open, setOpen] = useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => {
        setOpen(false);
        dispatch(goalActionCompleted(currentGoal))
    }
    const confetti = () =>{
        if(open || goalCompleted) return <Confetti />;
        else return "";
    }

    return (
        <>
            <LoadingComponent/>
            {confetti()}
            <AppContainer>
                <div className="pageContainer">
                    {hasGoal ?
                        <>
                            <h1>Current goal</h1>
                            <div sx={{ display: 'flex'}}>
                                <h5 sx={{mb: 0}}>Progress:</h5>
                                <Progress/>
                            </div>
                            <h4>Workouts:</h4>
                            <WorkoutList />
                        </>
                        :
                        <>
                            <h4>Programs</h4>
                            <ProgramList />
                            <br/>
                        </>
                    }
                    <br/><br/>
                    <h4>Completed goals:</h4>
                    {completedGoals.length ?
                        <CompletedGoalList/>
                        :
                        <p>You have no completed goals</p>
                    }
                </div>
            </AppContainer>
            <div>
                <Modal
                    open={open || goalCompleted}
                    onClose={handleClose}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                >
                    <Box sx={style}>
                        <div>

                            <h3>Congratulations!</h3>
                            <br/>
                           You have completed your goal. Do you want to create a new goal?</div>
                        <br/>
                        <Button variant="contained" onClick={handleRouteGoal}>New goal</Button>
                    </Box>
                </Modal>
            </div>
        </>
    );
};

export default GoalPage;