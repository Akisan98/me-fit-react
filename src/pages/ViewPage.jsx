import OverViewExeciseComponent from "../components/Overview/ExerciseComponent";
import OverviewWorkoutComponents from "../components/Overview/WorkoutComponent";
import { Container } from '@mui/material';
import LoadingComponent from "../components/UserFeedback/LoadingComponent";
import OverviewViewPrograms from "../components/Overview/ProgramComponent";
import AddoverviewComponent from "../components/Overview/AddWorkoutComponent";
import OverviewAddProgram from "../components/Overview/AddProgramComponent";

const ViewPage = () => {


    return (
        <Container>
            <div className="pageContainer">
                <LoadingComponent/>
                <OverViewExeciseComponent/>
                <br/>
                <br/>
                <h1>Workouts</h1>
                <AddoverviewComponent/>
                <OverviewWorkoutComponents/>
                <br/>
                <br/>
                <h1>Programs</h1>
                <OverviewAddProgram/>
                <OverviewViewPrograms/>
                <br/><br/>
            </div>
        </Container>

    );
}

/*const mapStateToProps = (state) => {
    console.log(state.exercises)
    return {
        auth: state.auth,
        requests: state.requests,
        exercises: state.exercises
    };
}*/


//export default withRouter(connect(mapStateToProps)(ViewPage));
export default ViewPage;