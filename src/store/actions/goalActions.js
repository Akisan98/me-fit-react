import * as types from '../constants/ActionTypes'

export const goalActionFetch = () => ({
    type: types.ACTION_GOAL_FETCH
})

export const goalActionSet = goals => ({
    type: types.ACTION_GOAL_SET,
    payload: goals
})

export const goalActionSelected = goal => ({
    type: types.ACTION_GOAL_SELECTED,
    payload: goal
})

export const goalActionInProcess = status => ({
    type: types.ACTION_GOAL_IN_PROCESS,
    payload: status
})

export const goalActionNew = () => ({
    type: types.ACTION_GOAL_NEW,
})


export const goalActionAdd = goal => ({
    type: types.ACTION_GOAL_ADD,
    payload: goal
})

export const goalActionCalcProgress = () => ({
    type: types.ACTION_GOAL_CALC_PROGRESS
})

export const goalActionSetProgress = progress => ({
    type: types.ACTION_GOAL_SET_PROGRESS,
    payload: progress
})

export const goalActionCompleted = goal => ({
    type: types.ACTION_GOAL_COMPLETED,
    payload: goal
})

export const goalActionChangeList = updatedGoal => ({
    type: types.ACTION_GOAL_CHANGE_LIST,
    payload: updatedGoal
})

export const goalActionCompletedStatus = () => ({
    type: types.ACTION_GOAL_COMPLETED_STATUS
})

export const goalActionTrue = goalId => ({
    type: types.ACTION_GOAL_TRUE,
    payload: goalId
})

export const goalActionModalOpen = (status) => ({
    type: types.ACTION_GOAL_MODAL_OPEN,
    payload: status
})

export const goalActionClear = () => ({
    type: types.ACTION_GOAL_CLEAR
})

export const goalActionDelete = id => ({
    type: types.ACTION_GOAL_DELETE,
    payload: id
})


export const goalActionError = error => ({
    type: types.ACTION_GOAL_ERROR,
    payload: error
})

export const goalWorkoutActionFetch = ids => ({
    type: types.ACTION_GOAL_WORKOUT_FETCH,
    payload: ids
})

export const goalWorkoutActionSet = workouts => ({
    type: types.ACTION_GOAL_WORKOUT_SET,
    payload: workouts
})

export const goalWorkoutActionComplete = workout => ({
    type: types.ACTION_GOAL_WORKOUT_COMPLETE,
    payload: workout
})

export const goalWorkoutActionCompletedList = workout => ({
    type: types.ACTION_GOAL_WORKOUT_COMPLETED_LIST,
    payload: workout
})

export const goalWorkoutActionChangeList = updatedWorkout => ({
    type: types.ACTION_GOAL_WORKOUT_CHANGE_LIST,
    payload: updatedWorkout
})

export const goalWorkoutActionFetchNew = workouts => ({
    type: types.ACTION_GOAL_WORKOUT_FETCH_NEW,
    payload: workouts
})


export const goalWorkoutActionError = error => ({
    type: types.ACTION_GOAL_WORKOUT_ERROR,
    payload: error
})




export const goalExerciseActionFetch = ids => ({
    type: types.ACTION_GOAL_EXERCISE_FETCH,
    payload: ids
})

export const goalExerciseActionSet = exercises => ({
    type: types.ACTION_GOAL_EXERCISE_SET,
    payload: exercises
})

export const createWorkoutFromProgram = payload => ({
    type: types.CREATE_GOALS_FROM_PROGRAM,
    payload
})


export const createEmptyGoal = payload => ({
    type: types.CREATE_GOALS_GOAL_PROGRAMGOALS,
    payload
})
export const fillEmptyGoal = payload => ({
    type: types.FILL_GOAL_PROGRAMGOALS,
    payload
})


