import * as types from '../constants/ActionTypes'

export const overviewActionFetchPrograms = () => ({
    type: types.ACTION_OVERVIEW_FETCH_PROGRAMS
})

export const overviewActionSetPrograms = programs => ({
    type: types.ACTION_OVERVIEW_SET_PROGRAMS,
    payload: programs
})

export const overviewActionFetchWorkouts = () => ({
    type: types.ACTION_OVERVIEW_FETCH_WORKOUTS
})

export const overviewActionSetWorkouts = workouts => ({
    type: types.ACTION_OVERVIEW_SET_WORKOUTS,
    payload: workouts
})

export const overviewActionFetchExercises = () => ({
    type: types.ACTION_OVERVIEW_FETCH_EXERCISES
})

export const overviewActionSetExercises = exercises => ({
    type: types.ACTION_OVERVIEW_SET_EXERCISES,
    payload: exercises
})

export const overviewActionError = error => ({
    type: types.ACTION_OVERVIEW_ERROR,
    payload: error
})

export const addExercise = payload => ({
    type: types.ACTION_OVERVIEW_ADD_EXERCISES,
    payload
})
export const getSets = () => ({
    type: types.ACTION_OVERVIEW_GET_SETS
})

export const setSets = (payload) => ({
    type: types.ACTION_OVERVIEW_SET_SETS,
    payload
})

export const createWorkout = (payload) => ({
    type: types.ACTION_OVERVIEW_CREATE_SETS,
    payload
})
export const createWorkoutFromSets = (payload) =>({
    type: types.ACTION_OVERVIEW_CREATE_WORKOUT,
    payload
})