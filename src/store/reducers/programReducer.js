import * as types from '../constants/ActionTypes'


const initialState = {
    programs: [],
    isLoading: true,
    error: ''
}

export const programReducer = (state = initialState , action) => {

    switch (action.type) {

        case types.ACTION_PROGRAM_FETCH:
            return {
                ...state,
                isLoading: true,
                loginError: ''
            }

        case types.ACTION_PROGRAM_SET:
            return {
                programs: action.payload,
                isLoading: true,
                loginError: ''
            }

        case types.ACTION_PROGRAM_NEW:
            return {
                ...state,
                newProgram: {id: 4, title: action.payload, exercises: [action.payload]},
                loginError: ''
            }

        case types.ACTION_PROGRAM_ADD:
            return {
                ...state,
                programs: [...state.programs, state.newProgram],
                loginError: ''
            }

        case types.ACTION_PROGRAM_DELETE:
            return {
                ...state,
                programs: [...action.payload],
            }

        case types.ACTION_PROGRAM_ERROR:
            return {
                ...state,
                programError: action.payload
            }
        case types.ACTION_PROGRAM_CREATE:
            return {
                ...state
            }
        case types.ACTION_PROGRAM_UPDATE:
            return {
                ...state
            }

        default:
            return state
    }
}

export const getAllPrograms = (state) => state.program.programs