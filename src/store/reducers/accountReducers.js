import * as types from "../constants/ActionTypes";

const initialState = {
    userId: -1,
    firstName: '',
    lastName: '',
    email: '',
    height: 0,
    weight: 0,
    fitnessLevel: 0,
    contributor: 0
}

export const accountReducer = (state = initialState, action) => {

    switch (action.type) {

        case types.ACTION_CREATE_ACCOUNT:
            return {
                ...state
            }

        // case types.ACTION_SET_ACCOUNT:
        //     return {
        //         ...state,
        //         userId: action.payload.userId,
        //         firstName: action.payload.firstName,
        //         lastName: action.payload.lastName,
        //         height: action.payload.height,
        //         weight: action.payload.weight,
        //         email: action.payload.email,
        //         fitnessLevel: action.payload.fitnessLevel,
        //         contributor: action.payload.contributor
        //     }
        default:
            return state
    }
};