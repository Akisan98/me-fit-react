import * as types from '../constants/ActionTypes'


const initialState = {
    workouts: [],
    isLoading: true,
    completed: false,
    error: ''
}

export const workoutReducer = (state = initialState , action) => {

    switch (action.type) {
        case types.ACTION_WORKOUT_UPDATE:
            return {
                ...state
            }
        case types.ACTION_WORKOUT_CREATE_SET_WORKOUT:
            return {
                ...state
            }

        case types.ACTION_WORKOUT_FETCH:
            return {
                ...state,
                isLoading: true,
                loginError: ''
            }

        case types.ACTION_WORKOUT_SET:
            return {
                workouts: action.payload,
                isLoading: true,
                loginError: ''
            }

        case types.ACTION_WORKOUT_ADD:
            return {
                ...state,
                workouts: [...action.payload, ...state],
                completed: false,
                loginError: ''
            }

        case types.ACTION_WORKOUT_DELETE:
            return state.filter((workout) => workout.id !== action.payload)


        case types.ACTION_WORKOUT_ERROR:
            return {
                ...state,
                workoutError: action.payload
            }

        default:
            return state
    }
}

export const getAllWorkouts = (state) => state.workout.workouts