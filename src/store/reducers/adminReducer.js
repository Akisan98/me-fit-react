import * as types from '../constants/ActionTypes'

const initialState = {
    requests: [],
    contributors: [],
    adminError: ''
}

export const adminReducer = (state = initialState , action) => {

    switch (action.type) {

        case types.ACTION_ADMIN_FETCH_CONTRIBUTORS:
            return {
                ...state,
            }

        case types.ACTION_ADMIN_SET_CONTRIBUTORS:
            return {
                ...state,
                contributors: action.payload
            }

        case types.ACTION_ADMIN_FETCH_REQUESTS:
            return {
                ...state
            }

        case types.ACTION_ADMIN_SET_REQUESTS:
            return {
                ...state,
                requests: action.payload
            }

        case types.ACTION_ADMIN_REPLY_REQUEST:
            const newRequestList = state.requests.filter(request =>
                request.id !== action.id);
            const newContributor = state.requests.filter(request =>
                request.id === action.id);

            return {
                ...state,
                requests: newRequestList,
                contributors: [...state.contributors, ...newContributor],
            }

        case types.ACTION_ADMIN_DELETE_CONTRIBUTOR:
            const newContributorsList = state.contributors.filter(contributor =>
                contributor.id !== action.id);
            return {
                ...state,
                contributors: newContributorsList,
            }

        case types.ACTION_ADMIN_ERROR:
            return {
                ...state,
                adminError: action.payload
            }

        default:
            return state
    }
}
