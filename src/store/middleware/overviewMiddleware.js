import * as types from '../constants/ActionTypes'
import {BASE_API} from "../api/api";
import {
    createWorkoutFromSets,
    overviewActionError,
    overviewActionSetExercises,
    overviewActionSetPrograms,
    overviewActionSetWorkouts,
    setSets
} from "../actions/overviewActions";
import {workoutActionFetch} from "../actions/workoutActions";
import {loginRefresh} from "../actions/authActions";

export const overviewMiddleware = ({ dispatch,getState }) => next => action => {
    next(action)
    const payload = action.payload;
    if (action.type === types.ACTION_OVERVIEW_FETCH_PROGRAMS) {
        fetch(BASE_API + '/api/v1/exercise')
            .then(r => r.json())
            .then(programs => {
                dispatch(overviewActionSetPrograms(programs))
            })
            .catch(error => {
                dispatch(overviewActionError(error.message))
            })
    }

    if (action.type === types.ACTION_OVERVIEW_FETCH_WORKOUTS) {
        fetch(BASE_API +'/api/v1/exercise')
            .then(r => r.json())
            .then(workouts => {
                dispatch(overviewActionSetWorkouts(workouts))
            })
            .catch(error => {
                dispatch(overviewActionError(error.message))
            })
    }

    if (action.type === types.ACTION_OVERVIEW_FETCH_EXERCISES) {
        //fetch(config.BASE_URL_DEV + '/api/v1/exercise')
        fetch(BASE_API + '/api/v1/exercise')
            .then(r => r.json())
            .then(exercises => {
                dispatch(overviewActionSetExercises(exercises))
            })
            .catch(error => {
                dispatch(overviewActionError(error.message))
            })
    }

    if (action.type === types.ACTION_OVERVIEW_ADD_EXERCISES){

        const body = {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'token' : payload.sessionToken
            },
            body: JSON.stringify(payload.exericseObjec)
        }
        //fetch(config.BASE_URL_DEV+"/api/v1/exercise",body)
        fetch(BASE_API + "/api/v1/exercise",body)
            .then(r => {
                r.json().then(exercise => {
                    let newExercise = payload.exercises;
                    newExercise.push(exercise)
                    dispatch(overviewActionSetExercises(newExercise))
                });
            })
    }

    if(types.ACTION_OVERVIEW_CREATE_SETS === action.type){
        console.log("DETTE ER SETTENE " + JSON.stringify(payload))

        const options = {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'token' : getState().auth.userToken
            },
            body: JSON.stringify(payload[0]["sets"])
        };

         fetch(BASE_API + "/api/v1/set/addMultiple", options)

            .then(r => r.json())
            .then(set => {
                console.log(JSON.stringify(set))
                payload[0]["sets"] = set
                dispatch(createWorkoutFromSets(payload))
            })
            .catch(error => {
                console.log("ERROR : " + JSON.stringify(error))
            })
    }

    if(types.ACTION_OVERVIEW_CREATE_WORKOUT === action.type){

        const options = {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'token' : getState().auth.userToken
            },
            body: JSON.stringify(payload)
        };

        fetch(BASE_API + "/api/v1/workout/Multiple", options)
            .then(r => r.json())
            .then(workout => {
                console.log("WORKOUT BLIR LAGET + " +JSON.stringify(workout))
                console.log(JSON.stringify(getState().workouts.workouts))
                dispatch(loginRefresh(getState().auth.userToken));
            })
            .catch(error => {
                console.log(error)
                //dispatch(userWorkoutActionError(error.message))
            })
    }

    if(types.ACTION_OVERVIEW_GET_SETS===action.type){

        fetch(BASE_API+"/api/v1/set")
            .then(response => response.json())
            .then(r => dispatch(setSets(r)))
    }
}
