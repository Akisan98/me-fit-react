import * as types from '../constants/ActionTypes'
import { BASE_API} from "../api/api";
import {
    adminActionError,
    adminActionSetContributor,
    adminActionSetRequest
} from "../actions/adminActions";

export const adminMiddleware = ({ dispatch, getState }) => next => action => {
    next(action)

    if(action.type === types.ACTION_ADMIN_FETCH_CONTRIBUTORS) {
        const body = {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'token' : getState().auth.userToken
            }

        }
        console.log("BODY 22" + JSON.stringify(body))
        fetch(BASE_API + "/api/v1/account/all",body)
            //fetch(adminApi)
            .then(r => r.json())
            .then(accounts => {
                const contributors = accounts.filter(account =>
                    account.contributer === 3);
                dispatch(adminActionSetContributor(contributors))
            })
            .catch(error => {
                dispatch(adminActionError(error.message))
            })
    }

    if(action.type === types.ACTION_ADMIN_FETCH_REQUESTS) {
        const body = {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'token' : getState().auth.userToken

            }
        }

        fetch(BASE_API + "/api/v1/account/all",body)
            //fetch(adminApi)
            .then(r => r.json())
            .then(accounts => {
                const contributors = accounts.filter(account =>
                    account.contributer === 2);
                dispatch(adminActionSetRequest(contributors))
            })
            .catch(error => {
                dispatch(adminActionError(error.message))
            })
    }

    if(action.type === types.ACTION_ADMIN_REPLY_REQUEST) {
        let statusId;
        if(action.status) {

            statusId = 3;
        }
        else {

            statusId = 1;
        }
        const options = {
            method: 'PATCH',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'token' : getState().auth.userToken
            },
            body: JSON.stringify({
                "contributer": statusId
            })
        };

        fetch(BASE_API + "/api/v1/account/" + action.id, options)
            .then(r => r.json())
            .then(r => console.log("Reply request admin respons " + r))
            .catch(error => {
                dispatch(adminActionError(error.message))
            })
    }

    if(action.type === types.ACTION_ADMIN_DELETE_CONTRIBUTOR) {
        fetch(BASE_API + "/api/v1/account/" + action.id, {method: 'DELETE'})
            //fetch(adminApi +'/' + action.id, {method: 'DELETE'})
            .then(r => console.log("Delete admin respons " + r))
            .catch(error => {
                dispatch(adminActionError(error.message))
            })
    }
}
