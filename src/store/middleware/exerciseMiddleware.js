import * as types from '../constants/ActionTypes'
import {BASE_API} from "../api/api";
import {exerciseActionError, exerciseActionSet} from "../actions/exerciseActions";

export const exerciseMiddleware = ({ dispatch }) => next => action => {
    next(action)

    if(action.type === types.ACTION_EXERCISE_FETCH) {
        fetch(`${BASE_API}/exercise`)
            .then(r => r.json())
            .then(exercises =>{
                dispatch(exerciseActionSet(exercises))
            })
            .catch(error => {
                dispatch(exerciseActionError(error.message))
            })
    }
    /*
    if(action.type === types.ACTION_EXERCISE_ERROR) {
    }*/
}