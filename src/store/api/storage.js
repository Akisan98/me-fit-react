import _workouts from './workouts.json'
import _programs from './programs.json'
import _exercises from './exercises.json'

const TIMEOUT = 100

export default {
    getWorkouts: (cb, timeout) => setTimeout(() => cb(_workouts), timeout || TIMEOUT),
    getPrograms: (cb, timeout) => setTimeout(() => cb(_programs), timeout || TIMEOUT),
    getExercises: (cb, timeout) => setTimeout(() => cb(_exercises), timeout || TIMEOUT)
}

