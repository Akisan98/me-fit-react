import Table from "@material-ui/core/Table";
import SelectedExerciseListItem from "../SelectedExerciseListItem/SelectedExerciseListItem";

const SelectedExerciseList = (props) => {
    const {tabIndex, exercise, setExercise} = props


    return (
        <Table>
            <tbody>
            <tr>
                <>
                    <th>Title</th>
                    <th>Sets</th>
                    <th>Reps</th>
                </>
            </tr>
            <SelectedExerciseListItem
                tabIndex={tabIndex}
                exercise={exercise}
                setExercise={setExercise}
            />
            </tbody>
        </Table>
    );
}
export default SelectedExerciseList;