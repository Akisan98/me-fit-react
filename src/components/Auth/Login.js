import React, { Component } from 'react';
import { GoogleLogin } from 'react-google-login';
import {connect } from "react-redux";
import { login } from "../../store/actions/authActions";
import config from '../../config.json';
import { withRouter, Redirect } from "react-router-dom";
import LoadingComponent from "../UserFeedback/LoadingComponent";



class Login extends Component {
    googleResponse = (response) => {
        if (!response.tokenId) {
            return;
        }

        this.props.login(response)
    };

    render() {


        const path = this.props.auth.weight===0?'/profile': '/dashboard';

        let content = !!this.props.auth.isAuthenticated ?
            (
                <Redirect to={{ pathname: path }}/>
            ) :
            (
                <GoogleLogin
                    clientId={config.GOOGLE_CLIENT_ID}
                    buttonText="Sign In with Google"
                    onSuccess={this.googleResponse}
                    onFailure={this.googleResponse}
                    cookiePolicy={'single_host_origin'}
                />
            );

        return (
            <div>
                <LoadingComponent/>
                <div className="splitScreen">
                    <div className="item">
                    </div>
                    <div className="item centerLogin">
                        <div>
                            <h2>Sign In</h2>
                            <br/><br/><br/>
                            {content}
                            <br/><br/>
                            <p>Register a Google account <a href="https://accounts.google.com/SignUp?hl=en">Here</a></p>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
};

const mapStateToProps = (state) => {
    return {
        auth: state.auth
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        login: (payload) => {
            dispatch(login(payload));
        }
    }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Login));
