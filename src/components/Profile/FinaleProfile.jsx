import {useSelector} from "react-redux";

const FinaleProfile = () => {
    const {firstName, lastName, height, weight, email, contributor, imageUrl} = useSelector(state => state.auth)



    return (
        <>
            <form>
                <div>
                    <br/>
                    <p>Firstname:<br/> <span style={{fontWeight: 700, fontSize: "1.2rem"}}>{firstName}</span></p>
                    <p>Lastname:<br/> <span style={{fontWeight: 700, fontSize: "1.2rem"}}>{lastName}</span></p>
                    <p>Email:<br/> <span style={{fontWeight: 700, fontSize: "1.2rem"}}>{email}</span></p>
                    <p>Height:<br/> <span style={{fontWeight: 700, fontSize: "1.2rem"}}>{height}</span></p>
                    <p>Weight:<br/> <span style={{fontWeight: 700, fontSize: "1.2rem"}}>{weight}</span></p>
                </div>
            </form>
        </>
    )
}

export default FinaleProfile