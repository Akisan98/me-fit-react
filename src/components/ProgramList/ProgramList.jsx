import {Table} from "@mui/material";
import ProgramListItem from "../ProgramListItem/ProgramListItem";

const ProgramList = () => {

    return (
        <Table>
            <tbody>
                <tr>
                    <th>Title</th>
                    <th>View details</th>
                    <th>Create goal from this</th>
                </tr>
                <ProgramListItem />
            </tbody>
        </Table>
    );
}
export default ProgramList;