import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import Paper from '@material-ui/core/Paper';

import nextId from "react-id-generator";
import './Calendar.css';
import {goalActionSelected, goalActionTrue} from "../../store/actions/goalActions";
import {useDispatch} from "react-redux";
import moment from "moment/moment";

const CalendarBody = props => {

    const {firstDayOfMonth, daysInMonth, currentDay, currentMonth, currentDate, currentMonthNum, selectedDay, goals, setSelectedDay, actualMonth, actualYear, weekdays, getWeek, hasGoal, inProcess } = props;

    let currMonthNum = currentMonthNum()+1
    const dispatch = useDispatch();

    let allGoalsDays = [];
    let goalWeek = [];
    for (let i = 0; i < goals.length; i++) {
        const day = goals[i].startDate.slice(8, 10).replace(/\b0+/g, '')
        const month = goals[i].startDate.slice(5, 7)
        const year = goals[i].startDate.slice(0, 4)
        if(!goals[i].completed) {
            if(hasGoal === false) {
                dispatch(goalActionTrue(goals[i].id))
            }
            const temp = getWeek(day, month, year);
            goalWeek = [...goalWeek, ...temp];
            //console.log(goalWeek.map(x => x));
        }
        else {
            const temp = getWeek(day, month, year);
            allGoalsDays = [...allGoalsDays, ...temp];
            //console.log(allGoalsDays.map(x => x));
        }
    }

    let blanks = [];
    for (let i = 0; i < firstDayOfMonth(); i++) {
        blanks.push(
            <TableCell key={nextId()}>{""}</TableCell>
        )
    }

    let monthDays = [];
    let setWeek = false;
    let days = 7
    for (let d = 1; d <= daysInMonth(); d++) {
        let currDay, selectDay, activeDay, goalDay;

        // Check if day is today
        if (currentDay() == d && currentMonth() === actualMonth()) currDay = "today";

        // Check if day is selected day
        if ((inProcess && selectedDay.day == d && currentMonthNum() === selectedDay.month) || (setWeek === true)) {
            selectDay = "selected-day";
            setWeek = true;
            days--
            if(days === 0) {
                setWeek = false
            }
        }

        // Check if day found in this month active days
        const day = ('0' + d).slice(-2)
        let formattedDate = `${actualYear()}-${currMonthNum}-${day}`;
        if (allGoalsDays.indexOf(formattedDate) !== -1) {
            activeDay = "active";
        }
        if (goalWeek.indexOf(formattedDate) !== -1) {
            goalDay = "goal-day";
        }

        monthDays.push(
            <TableCell
                key={d}
                className={`week-day ${currDay} ${selectDay} ${goalDay}`}

                onClick={() => {
                    if(!hasGoal && !moment(formattedDate).isBefore(currentDate)) {
                        dispatch(goalActionSelected(formattedDate))
                        setSelectedDay(d)
                    }
                }}
            >
                <span className={activeDay}>{d}</span>
            </TableCell>
        );
    }

    let totalSlots = [...blanks, ...monthDays];
    let rows = [];
    let cells = [];

    totalSlots.forEach((row, i) => {
        if (i % 7 !== 0) {
            cells.push(row);
        } else {
            rows.push(cells);
            cells = [];
            cells.push(row);
        }
        if (i === totalSlots.length - 1) {
            rows.push(cells)
        }
    })

    return (
        <>
            <TableContainer component={Paper}>
                <Table className="calendar">
                    <TableHead>
                        <TableRow>
                            {
                                weekdays.map((day, i) => (
                                    <TableCell key={i}>
                                        {day}
                                    </TableCell>
                                ))
                            }
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {
                            rows.map((day, i) =>
                                <TableRow
                                    key={i}
                                >
                                    {day}
                                </TableRow>)
                        }
                    </TableBody>
                </Table>
            </TableContainer>
        </>
    )
}

export default CalendarBody;