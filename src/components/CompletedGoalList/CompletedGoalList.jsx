import {Table} from "@mui/material";
import CompletedGoalListItem from "../CompletedGoalListItem/CompletedGoalListItem";

const CompletedGoalList = () => {

    return (
        <Table>
            <tbody>
            <tr className="coloredHeader">
                <th className="headerText">Title</th>
                <th className="headerText">Start date</th>
                <th className="headerText"></th>
                <th></th>
            </tr>
            <CompletedGoalListItem />
            </tbody>
        </Table>
    )
}

export default CompletedGoalList;