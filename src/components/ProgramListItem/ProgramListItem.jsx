import {useState} from "react";
import {connect, useDispatch, useSelector} from "react-redux";
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import * as React from "react";
import TabContext from "@mui/lab/TabContext";
import TabList from "@mui/lab/TabList";
import Tab from "@mui/material/Tab";
import TabPanel from "@mui/lab/TabPanel";
import TableContainer from "@mui/material/TableContainer";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import TableBody from "@mui/material/TableBody";
import {withRouter} from "react-router-dom";
import Stack from '@mui/material/Stack';
import TextField from '@mui/material/TextField';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import DesktopDatePicker from '@mui/lab/DesktopDatePicker';
import Button from "@mui/material/Button";
import {createWorkoutFromProgram} from "../../store/actions/goalActions";

const ProgramListItem = (props) => {
    const dispatch = useDispatch();
    const {programs} = useSelector(state => state.programs)
    const [clickedProgramID,setClickedProgramID] = useState(0);
    const [value, setValue] = useState(1);
    const [dateValue, setDateValue] = useState(new Date());
    const [open, setOpen] = useState(false);
    const [open2, setOpen2] = useState(false);
    const [programTitle, setProgramTitle] = useState("");
    const [programCategory, setProgramCategory] = useState("strength");
    const handleOpen = () => setOpen(true);
    const handleOpen2 = () => setOpen2(true);
    const handleClose = () =>{
        setOpen(false);
        setValue(1);
    }
    const handleClose2 = () =>{
        setOpen2(false);
        setValue(1);
    }
    const convertDate = str => {
        str = str.toString();
        let parts = str.split(" ");
        let months = {
            Jan: "01",
            Feb: "02",
            Mar: "03",
            Apr: "04",
            May: "05",
            Jun: "06",
            Jul: "07",
            Aug: "08",
            Sep: "09",
            Oct: "10",
            Nov: "11",
            Dec: "12"
        };
        return parts[3] + "-" + months[parts[1]] + "-" + parts[2];
    };


    const createGoal = () => {
        let selectedProgram = [{}];
        for(const program of props.programs){
            if(program.id === clickedProgramID){
                selectedProgram = program;
            }
        }
        let workoutId = selectedProgram.workouts;
        let workouts = [];
        for (let i = 0; i < workoutId.length; i++) {
            for (let j = 0; j < props.workouts.length; j++) {
                if(props.workouts[j].id==workoutId[i]){
                    let thing = JSON.parse(JSON.stringify(props.workouts[i]))
                    thing["madeByContributor"] = false
                    thing["completed"] = 0;
                    delete thing.id;
                    workouts.push(thing)
                }
            }
        }
        const payload = {
            workouts,
            date : convertDate(dateValue),
            title: programTitle,
            category : programCategory
        }

        console.log(JSON.stringify(payload))
        dispatch(createWorkoutFromProgram(payload))

    }
    const handleChange = (event, newValue) => {
        setValue(newValue);
    };
    const handleDateChange = ( newValue) => {
        setDateValue(newValue);
    };

    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 900,
        bgcolor: 'background.paper',
        border: '2px solid #000',
        boxShadow: 24,
        p: 4,
    };

    const sets = (displayWorkoutId) => {
        let setId = []
        for (const workout of props.workouts) {
            if(workout.id===displayWorkoutId){
                for(const id of workout.sets){
                    setId.push(id);
                }
            }
        }
        let row = []
        for(const id of setId){
            for (const set of props.sets){
                if(id === set.id){
                    row.push({exercise: set.exercise.name, sets: set.sets, reps: set.reps})
                }
            }
        }
        return(
            <TableBody >
                {row.map((row) => (
                    <TableRow key={row.id}>
                        <TableCell component="th" scope="row">
                            {row.exercise}
                        </TableCell>
                        <TableCell>{row.sets}</TableCell>
                        <TableCell>{row.reps}</TableCell>
                    </TableRow>
                ))}
            </TableBody>
        )
    }

    const selectedProgramView = (selected) => {
        let day = 0

        if(clickedProgramID !== 0){return (
            selected.workouts.map((field) => (
                <Tab label={"Workout " + ++day} value={day}/>
            )))}
        return ""
    }

    const viewProgramInfo = (selected) => {
        if(clickedProgramID!== 0){
            let day = 1
            return (selected.workouts.map((field) => (
                <TabPanel value={day++} >
                    <TableContainer component={Paper}>
                        <Table sx={{ minWidth: 300 }} wrapperStyle={{ maxHeight: 100 }}>
                            <TableHead align={"center"}>
                                <TableRow style={{cursor:'pointer'}}  align={"center"} >
                                    <TableCell>Exercise</TableCell>
                                    <TableCell>Reps</TableCell>
                                    <TableCell>Sets</TableCell>
                                </TableRow>
                            </TableHead>
                            {sets(field)}
                        </Table>
                    </TableContainer>
                </TabPanel>
            )))}
    }

    const programSpecs = () => {
        let selectedProgram = [{}];
        for(const program of props.programs){
            if(program.id === clickedProgramID){
                selectedProgram = program;
            }
        }
        return (
            <>
                <h1 align={"center"}>{selectedProgram.title}</h1>
                <Box sx={{ width: '100%', typography: 'body1' }}>
                    <TabContext value={value}>
                        <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                            <TabList onChange={handleChange} aria-label="lab API tabs example">
                                {selectedProgramView(selectedProgram)}
                            </TabList>
                        </Box>

                        {viewProgramInfo(selectedProgram)}

                    </TabContext>
                </Box>
            </>
        )
    }

    const createProgram = () => {
        return(
            <>
                <TextField onChange={(e)=>setProgramTitle(e.target.value)} placeholder={"Title of the goal"} id="standard-basic" label="Goal title" variant="standard" />
                <br/><br/>
                <h5>Choose startdate</h5>
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                    <Stack spacing={3}>
                        <DesktopDatePicker
                            inputFormat="dd/MM/yyyy"
                            value={dateValue}
                            onChange={handleDateChange}
                            renderInput={(params) => <TextField {...params} />}
                        />
                    </Stack>
                </LocalizationProvider>
                <br/>
                <Button variant="contained" onClick={createGoal} style={{textAlign: 'center'}}>CREATE GOAL</Button>
            </>
        )
    }

    return(
        <>
            {programs.map(program =>
                <tr  key={program.id}>
                    <td>{program.title}</td>
                    <td><Button style={{margin:"0.25rem"}} className="custom-button" onClick={() =>{handleOpen();  setClickedProgramID(program.id)}}>View Details</Button></td>
                    <td><Button className="custom-button" onClick={() =>{handleOpen2();  setClickedProgramID(program.id)}}>Create goal from this program</Button></td>
                </tr>

            )}

            {/*VIEW DETAILS MODAL*/}
            <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    {programSpecs()}
                </Box>
            </Modal>


            {/*CREATE GOAL MODAL*/}
            <Modal
                open={open2}
                onClose={handleClose2}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    {createProgram()}
                </Box>
            </Modal>
        </>
    )
}

const mapStateToProps = (state) => {
    return {
        auth: state.auth,
        exercises: state.exercises,
        workouts: state.workouts.workouts,
        sets : state.overview.sets,
        programs: state.programs.programs
    };
}


export default withRouter(connect(mapStateToProps)(ProgramListItem));