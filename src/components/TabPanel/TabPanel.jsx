import * as React from 'react';
import PropTypes from 'prop-types';
import {
    DialogContent,
    Tabs,
    Tab,
    Typography,
    Box,
    Button,
    DialogActions,
    TextField
} from "@mui/material";
import AvailableExerciseList from "../AvailableExerciseList/AvailableExerciseList";
import SelectedExerciseList from "../SelectedExerciseList/SelectedExerciseList";
import {useDispatch, useSelector} from "react-redux";
import {
    userGoalActionAdded,
    userGoalActionAddProgramTitle,
    userGoalActionResetModalInput,
    userSetActionCreate,
    userWorkoutActionAdd
} from "../../store/actions/userGoalsActions";
import {useEffect, useState} from "react";
import {goalActionFetch, goalActionModalOpen} from "../../store/actions/goalActions";
import TypeSelect from "../TypeSelect/TypeSelect";

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box sx={{ p: 3}}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.number.isRequired,
    value: PropTypes.number.isRequired,
};

function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}

export default function BasicTabs(props) {
    const {open, handleClose, handleModalReset} = props;
    const [tabWorkoutType, setTabWorkoutType] = useState(new Array(7));
    const [value, setValue] = React.useState(0);
    const [type, setType] = React.useState("Do not want to specify");
    const [programTitle, setProgramTitle] = useState('')
    const [exercise, setExercise] = useState({0: [], 1: [], 2: [], 3: [], 4: [], 5: [], 6: []});
    const dispatch = useDispatch();
    const {selectedExercises, program} = useSelector(state => state.userGoal);

    useEffect(() => {
        console.log("ExerciseState: " + JSON.stringify(exercise))
        console.log("Value: " + value)
        console.log("Dette er program: " + JSON.stringify(program))
    })

    const setStateOfParent = (data) => {
        setType(data);
    }

    const handleChange = (event, newValue) => {
        if (selectedExercises.length) {
            dispatch(userWorkoutActionAdd(selectedExercises, value, type))
            setValue(newValue);
        } else {
            setValue(newValue);
        }
    };

    const handleProgramEmpty = () => {
        return !program.length;
    }

    const setStateOfWorkoutType = (data) => {
        setTabWorkoutType(data);
    }

    const saveProgram = () => {
        dispatch(goalActionModalOpen(open))
        dispatch(goalActionFetch())
        dispatch(userSetActionCreate(exercise, tabWorkoutType))
        dispatch(userGoalActionAdded())
        dispatch(userGoalActionAddProgramTitle(programTitle))
        dispatch(userGoalActionResetModalInput())
        //dispatch(setActionCreate(exercise))
    }

    return (
        <Box sx={{width: '100%'}}>
            <Box sx={{borderBottom: 1, borderColor: 'divider'}}>
                <Tabs value={value} onChange={handleChange} aria-label="basic tabs example">
                    <Tab label="Day 1" {...a11yProps(0)} />
                    <Tab label="Day 2" {...a11yProps(1)} />
                    <Tab label="Day 3" {...a11yProps(2)} />
                    <Tab label="Day 4" {...a11yProps(3)} />
                    <Tab label="Day 5" {...a11yProps(4)} />
                    <Tab label="Day 6" {...a11yProps(5)} />
                    <Tab label="Day 7" {...a11yProps(6)} />
                    <Tab label="Finish registration" {...a11yProps(7)} />
                </Tabs>
            </Box>
            <TabPanel value={value} index={0}>
                <DialogContent dividers>
                    <TypeSelect
                        tabIndex={value}
                        type={type}
                        setStateOfParent={setStateOfParent}
                        tabWorkoutType={tabWorkoutType}
                        setStateOfWorkoutType={setStateOfWorkoutType}
                    /><br/>
                    <h4>Selected exercises</h4>
                    <Typography gutterBottom>
                        <SelectedExerciseList
                            tabIndex={value}
                            exercise={exercise}
                            setExercise={setExercise}/>
                    </Typography>
                    <br/>
                    <h4>Available exercises</h4>
                    <Typography gutterBottom>
                        <AvailableExerciseList tabIndex={value}/>
                    </Typography>
                </DialogContent>
            </TabPanel>
            <TabPanel value={value} index={1}>
                <DialogContent dividers>
                    <TypeSelect
                        tabIndex={value}
                        type={type}
                        setStateOfParent={setStateOfParent}
                        tabWorkoutType={tabWorkoutType}
                        setStateOfWorkoutType={setStateOfWorkoutType}
                    /><br/>
                    <h4>Selected exercises</h4>
                    <Typography gutterBottom>
                        <SelectedExerciseList
                            tabIndex={value}
                            exercise={exercise}
                            setExercise={setExercise}
                        />
                    </Typography>
                    <br/>
                    <h4>Available exercises</h4>
                    <Typography gutterBottom>
                        <AvailableExerciseList tabIndex={value}/>
                    </Typography>
                </DialogContent>
            </TabPanel>
            <TabPanel value={value} index={2}>
                <DialogContent dividers>
                    <TypeSelect
                        tabIndex={value}
                        type={type}
                        setStateOfParent={setStateOfParent}
                        tabWorkoutType={tabWorkoutType}
                        setStateOfWorkoutType={setStateOfWorkoutType}
                    /><br/>
                    <h4>Selected exercises</h4>
                    <Typography gutterBottom>
                        <SelectedExerciseList
                            tabIndex={value}
                            exercise={exercise}
                            setExercise={setExercise}
                        />
                    </Typography>
                    <br/>
                    <h4>Available exercises</h4>
                    <Typography gutterBottom>
                        <AvailableExerciseList tabIndex={value}/>
                    </Typography>
                </DialogContent>
            </TabPanel>
            <TabPanel value={value} index={3}>
                <DialogContent dividers>
                    <TypeSelect
                        tabIndex={value}
                        type={type}
                        setStateOfParent={setStateOfParent}
                        tabWorkoutType={tabWorkoutType}
                        setStateOfWorkoutType={setStateOfWorkoutType}
                    /><br/>
                    <h4>Selected exercises</h4>
                    <Typography gutterBottom>
                        <SelectedExerciseList
                            tabIndex={value}
                            exercise={exercise}
                            setExercise={setExercise}
                        />
                    </Typography>
                    <br/>
                    <h4>Available exercises</h4>
                    <Typography gutterBottom>
                        <AvailableExerciseList tabIndex={value}/>
                    </Typography>
                </DialogContent>
            </TabPanel>
            <TabPanel value={value} index={4}>
                <DialogContent dividers>
                    <TypeSelect
                        tabIndex={value}
                        type={type}
                        setStateOfParent={setStateOfParent}
                        tabWorkoutType={tabWorkoutType}
                        setStateOfWorkoutType={setStateOfWorkoutType}
                    /><br/>
                    <h4>Selected exercises</h4>
                    <Typography gutterBottom>
                        <SelectedExerciseList
                            tabIndex={value}
                            exercise={exercise}
                            setExercise={setExercise}
                        />
                    </Typography>
                    <br/>
                    <h4>Available exercises</h4>
                    <Typography gutterBottom>
                        <AvailableExerciseList tabIndex={value}/>
                    </Typography>
                </DialogContent>
            </TabPanel>
            <TabPanel value={value} index={5}>
                <DialogContent dividers>
                    <TypeSelect
                        tabIndex={value}
                        type={type}
                        setStateOfParent={setStateOfParent}
                        tabWorkoutType={tabWorkoutType}
                        setStateOfWorkoutType={setStateOfWorkoutType}
                    /><br/>
                    <h4>Selected exercises</h4>
                    <Typography gutterBottom>
                        <SelectedExerciseList
                            tabIndex={value}
                            exercise={exercise}
                            setExercise={setExercise}
                        />
                    </Typography>
                    <br/>
                    <h4>Available exercises</h4>
                    <Typography gutterBottom>
                        <AvailableExerciseList tabIndex={value}/>
                    </Typography>
                </DialogContent>
            </TabPanel>
            <TabPanel value={value} index={6}>
                <DialogContent dividers>
                    <TypeSelect
                        tabIndex={value}
                        type={type}
                        setStateOfParent={setStateOfParent}
                        tabWorkoutType={tabWorkoutType}
                        setStateOfWorkoutType={setStateOfWorkoutType}
                    /><br/>
                    <h4>Selected exercises</h4>
                    <Typography gutterBottom>
                        <SelectedExerciseList
                            tabIndex={value}
                            exercise={exercise}
                            setExercise={setExercise}
                        />
                    </Typography>
                    <br/>
                    <h4>Available exercises</h4>
                    <Typography gutterBottom>
                        <AvailableExerciseList tabIndex={value}/>
                    </Typography>
                </DialogContent>
            </TabPanel>
            <TabPanel value={value} index={7} handleClose={handleClose}>
                <DialogContent>
                    <Typography gutterBottom>
                        <Box>
                            <h5>What would you like to call this goal?</h5>
                            <TextField id="goal-title" onChange={event => setProgramTitle(event.target.value)}
                                       label="Goal title" variant="filled"/>
                            <div style={{
                                marginTop: "2rem",
                                padding: "0.5rem",
                                borderRadius: "1rem",
                                backgroundColor: "lightblue"
                            }}>
                                <p>If you are pleased with your setup, click the "<b>Save Changes</b>" button.</p>
                                <p><b>NB</b>: Your changes will be lost if not clicked!</p>
                            </div>
                        </Box>
                    </Typography>
                    <DialogActions>
                        <Button variant="outlined" color="error" autoFocus onClick={() => {handleClose(); handleModalReset()}}>
                            Close
                        </Button>
                        <Button disabled={handleProgramEmpty()} variant="outlined" autoFocus onClick={() => {
                            handleClose();
                            saveProgram()
                        }}>
                            Save changes
                        </Button>
                    </DialogActions>
                </DialogContent>
            </TabPanel>
        </Box>
    );
}