import {
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Paper,
    Box,
    Modal
} from '@mui/material';
import {Button} from "react-bootstrap";
import {withRouter} from "react-router-dom";
import {connect, useDispatch} from "react-redux";
import {useState} from "react";
import * as React from 'react';
import {createNewSetsInWorkout} from "../../store/actions/workoutActions";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

const OverViewExerciseComponent = (props) => {
    const [viewExercises, setViewExercises] = useState(false);
    const [displayWorkout,setDisplayWorkout] = useState(false);
    const [filterValue,setFilterValue] = useState("none");
    const [displayWorkoutId,setDisplayWorkoutId] = useState(0);
    const listOptions = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]

    const [newExercise,setNewExercise] = useState(0)
    const [newExerciseSets,setNewExerciseSets] = useState(1)
    const [newExerciseReps,setNewExerciseReps] = useState(1)
    const [newlyAddedSets,setNewlyAddedSets] = useState([])
    const [deletedSet,setDeletedSet] = useState([])
    const dispatch = useDispatch()

    const saveChanges = () => {
        let workout = {};
        for (let i = 0; i < props.workouts.length; i++) {
            console.log("comparing " + props.workouts[i].id + " "+ displayWorkoutId)
            if(props.workouts[i].id == displayWorkoutId){
                workout = props.workouts[i]
            }
        }
        let sets = workout.sets;
        //LIST over prev sets in a workout

        let newSets = []
        for (let i = 0; i < sets.length; i++) {
            let add = true;
            for (let j = 0; j < deletedSet.length; j++) {
                if(sets[i]==deletedSet[j]){
                    add = false
                    break;
                }
            }
            if(add) newSets.push(sets[i]);
        }
        const payload = {
            oldSets : newSets,
            notCreatedSets : newlyAddedSets,
            workoutId: displayWorkoutId
        }
        console.log("Payload" + JSON.stringify(payload));
        dispatch(createNewSetsInWorkout(payload));
        handleClose()
    }

    const addExercise = () => {
        setNewlyAddedSets([...newlyAddedSets,{exerciseId:newExercise,sets:newExerciseSets,reps:newExerciseReps}])
    }
    const changeSet = () => {
        if(deletedSet.length!==0 || newlyAddedSets.length !== 0){
            return(
                <Button className="custom-button" onClick={()=>saveChanges()}>Save changes</Button>
            )
        }
        return (
            ""
        )
    }

    const workouts = () =>{
        return(
            <>
                <TableContainer style={{ maxHeight: 500 }}  component={Paper}>
                    <Table stickyHeader sx={{ minWidth: 300 }} wrapperStyle={{ maxHeight: 100 }}>
                        <TableHead  style={{zIndex:1}} align={"center"}>
                            <TableRow style={{cursor:'pointer'}}  align={"center"} >
                                <TableCell>Exercise</TableCell>
                                <TableCell>Reps</TableCell>
                                <TableCell>Sets</TableCell>
                            </TableRow>
                        </TableHead>
                        {sets()}
                    </Table>
                </TableContainer>
                {contributorEditArea()}

            </>

        )
    }
    const contributorEditArea = () =>{
        if(props.auth.contributor===3||props.auth.admin===3){
            return(
                <>
                    <br/>
                    <select onChange={(e) => setNewExercise(e.target.value)} name="" id="">
                        {props.exercises.exercises.map((row)=>(
                            <option value={row.id}>{row.name}</option>
                        ))}
                    </select>
                    <select onChange={(e) => setNewExerciseSets(e.target.value)} name="" id="">
                        {listOptions.map((row)=>(
                            <option value={row}>{row}</option>
                        ))}
                    </select>
                    <select onChange={(e) => setNewExerciseReps(e.target.value)} name="" id="">
                        {listOptions.map((row)=>(
                            <option value={row}>{row}</option>
                        ))}
                    </select>
                    <br/><br/>
                    <Button className="custom-button" onClick={()=>addExercise() }>Add exercise</Button>
                    <br/>

                    <br/>
                    {changeSet()}
                </>
            )
        }
        else return "";
    }
    const deleteButton = (id) =>{
        if(props.auth.contributor===3||props.auth.admin===3){
            return (<Button className="cancel-button"  onClick={()=>setDeletedSet([...deletedSet,id])}>Delete</Button>)
        }
        return ""
    }
    const sets = () => {
        let setId = []
        for (const workout of props.workouts) {
            if(workout.id===displayWorkoutId){
                for(const id of workout.sets){
                    setId.push(id);
                }
            }
        }

        let row = []
        for(const id of setId){
            for (const set of props.sets){
                if(id === set.id){
                    row.push({exercise: set.exercise.name, sets: set.sets, reps: set.reps,id:id})
                }
            }
        }
        let newRow = [];
        for (let i = 0; i < row.length; i++) {
            let add = true;
            for (let j = 0; j < deletedSet.length; j++) {
                if(deletedSet[j] === row[i].id){
                    add = false
                }
            }
            if(add)newRow.push(row[i]);
        }

        let createdSets = []
        let newlyAddedSets2 = JSON.parse(JSON.stringify(newlyAddedSets));
        for (let i = 0; i < newlyAddedSets2.length; i++) {
            for (let j = 0; j < props.exercises.exercises.length; j++) {
                if(props.exercises.exercises[j].id == newlyAddedSets2[i].exerciseId){
                    newlyAddedSets2[i]["name"] = props.exercises.exercises[j].name;
                    createdSets.push( newlyAddedSets2[i]);
                }
            }
        }

        return(
            <TableBody >
                {newRow.map((field) => (
                    <TableRow key={field.id}>
                        <TableCell component="th" scope="row">
                            {field.exercise}
                        </TableCell>
                        <TableCell>{field.sets}</TableCell>
                        <TableCell>{field.reps}</TableCell>
                        {deleteButton(field.id)}
                    </TableRow>
                ))}
                {createdSets.map((field)=>(
                    <TableRow key={field.exerciseId}>
                        <TableCell component="th" scope="row">
                            {field.name}
                        </TableCell>
                        <TableCell>{field.sets}</TableCell>
                        <TableCell>{field.reps}</TableCell>
                        <Button className="cancel-button" onClick={()=>
                            setNewlyAddedSets(newlyAddedSets.filter((set)=>(
                                set.id!==field.id
                            )))}>Delete</Button>
                    </TableRow>
                ))}
            </TableBody>
        )
    }

    const [open, setOpen] = React.useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => {setOpen(false)
        setNewExercise(0)
        setNewExerciseSets(0)

        setNewlyAddedSets([])
        setDeletedSet([])
    };

    const onclickViewExercises = () => {
        if(viewExercises){
            setViewExercises(false)
        }
        else{
            setViewExercises(true)
        }
    }

    const workoutClicked = (id) =>{
        setDisplayWorkout(true)
        setDisplayWorkoutId(id)
        handleOpen()
    }

    const renderWorkouts = () => {
        if (viewExercises){

            let liste = [];

            if(filterValue!== "none"){
                liste = JSON.parse(JSON.stringify(props.workouts))
                liste = liste.filter((workout)=> workout.madeByContributor===true)
                liste = liste.filter((workout) => workout.type.includes(filterValue))
            }
            else{
                liste = JSON.parse(JSON.stringify(props.workouts))
                liste = liste.filter((workout)=> workout.madeByContributor===true)
            }
            return(
                liste.map((field) => (
                    <TableRow style={{cursor:'pointer'}} key={field.id}>
                        <TableCell  onClick={()=>workoutClicked(field.id)}>{field.title} </TableCell>
                        <TableCell  onClick={()=>workoutClicked(field.id)}>{field.type} </TableCell>
                    </TableRow>
                ))
            );
        }
        else {return ""}
    }

    const renderExerciseHead = () =>{
        if(viewExercises){
            return(
                <TableHead className="coloredHeader">
                    <TableRow style={{cursor:'pointer'}} onClick={onclickViewExercises}>
                        <TableCell className="headerText">Workout name</TableCell>
                        <TableCell className="headerText">Workout type</TableCell>
                    </TableRow>
                </TableHead>
            )
        }
        else {
            return(
                <TableHead className="coloredHeader">
                    <TableRow style={{cursor:'pointer'}} onClick={onclickViewExercises}>
                        <TableCell className="headerText" align={"center"}>Click to view Workouts</TableCell>
                    </TableRow>
                </TableHead>
            )
        }
    }

    return (
        <div>
            {viewExercises &&
                <>
                    <b>Filter by Type&nbsp;&nbsp;&nbsp;&nbsp;</b>
                    <select onChange={(e)=> setFilterValue(e.target.value)}>
                        <option value="none">No Filter</option>
                        <option value="strength">Strength</option>
                        <option value="balance">Balance</option>
                        <option value="flexibility and mobility">Flexibility and Mobility</option>
                        <option value="aerobics">Aerobics</option>
                        <option value="coordination and agility">Coordination and Agility</option>
                    </select>
                    <br/><br/>
                </>
            }

            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                    {renderExerciseHead()}
                    {renderWorkouts()}
                </Table>
            </TableContainer>
            <div>
                <Modal
                    open={open}
                    onClose={handleClose}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                >
                    <Box sx={style}>
                        {workouts()}
                    </Box>
                </Modal>
            </div>
        </div>
    );
}

const mapStateToProps = (state) => {
    return {
        auth: state.auth,
        exercises: state.exercises,
        workouts: state.workouts.workouts,
        sets : state.overview.sets
    };
}

export default withRouter(connect(mapStateToProps)(OverViewExerciseComponent));