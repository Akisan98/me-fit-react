import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {useState} from "react";
import OverviewAddExercise from "./AddExerciseComponent";

const ExerciseComponent = (props) => {
    const [viewExercises, setViewExercises] = useState(false);
    const [filterMuscles, setFilterMuscles] = useState("none")

    const onclickViewExercises = () => {
        if(viewExercises){
            setViewExercises(false)
        }
        else{
            setViewExercises(true)
        }
    }

    const filterValues = (muscles) => {
        setFilterMuscles(muscles)
    }

    const renderExceriseses = () => {
        if (viewExercises){
            let list = []
            if(filterMuscles!=="none"){
                list = JSON.parse(JSON.stringify(props.exercises.exercises))
                list = list.filter((value) => value.muscules.toLowerCase().includes(filterMuscles))
            }
            else {
                list = props.exercises.exercises;
            }
            return(
                <TableBody>
                    {list.map((row) => (
                        <TableRow key={row.id}>
                            <TableCell component="th" scope="row">
                                {row.name}
                            </TableCell>
                            <TableCell>{row.muscules}</TableCell>
                            <TableCell>{row.description}</TableCell>
                            <TableCell><a href={row.videoUrl}>{row.videoUrl}</a></TableCell>

                        </TableRow>
                    ))}
                </TableBody>
            );
        }
        else {return ""}
    }

    const renderExerciseHead = () =>{
        if(viewExercises){
            return(
                <TableHead className="coloredHeader">
                    <TableRow style={{cursor:'pointer'}} onClick={onclickViewExercises}>
                        <TableCell className="headerText">Exercises</TableCell>
                        <TableCell className="headerText">Muscles</TableCell>
                        <TableCell className="headerText">Description</TableCell>
                        <TableCell className="headerText">Video</TableCell>
                    </TableRow>
                </TableHead>
            )
        }
        else {

            return(
                <TableHead className="coloredHeader" >
                    <TableRow style={{cursor:'pointer'}} onClick={onclickViewExercises}>
                        <TableCell align={"center"} className="headerText">Click to view exercises</TableCell>
                    </TableRow>
                </TableHead>
            )
        }
    }


    return (
        <div>

            <br/>
            <h1>Exercises</h1>
            <OverviewAddExercise/>
            {viewExercises &&
            <>
                <b>Filter by Muscle&nbsp;&nbsp;&nbsp;&nbsp;</b>

                <select onChange={e => filterValues(e.target.value)} defaultValue={"none"}>
                    <option value="none">No Filter</option>
                    <option value="biceps">Biceps</option>
                    <option value="triceps">Triceps</option>
                    <option value="chest">Chest</option>
                    <option value="back">Back</option>
                    <option value="shoulders">Shoulders</option>
                    <option value="calves">Calves</option>
                    <option value="glutes">Glutes</option>
                    <option value="legs">Legs</option>
                </select>
                <br/><br/>
            </>
            }
            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                    {renderExerciseHead()}
                    {renderExceriseses()}
                </Table>
            </TableContainer>

        </div>
    );
}

const mapStateToProps = (state) => {
    return {
        auth: state.auth,
        exercises: state.exercises
    };
}


export default withRouter(connect(mapStateToProps)(ExerciseComponent));