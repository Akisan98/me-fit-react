import Table from '@mui/material/Table';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import {withRouter} from "react-router-dom";
import {connect, useDispatch} from "react-redux";
import {useState} from "react";
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import Tab from '@mui/material/Tab';
import TabContext from '@mui/lab/TabContext';
import TabList from '@mui/lab/TabList';
import TabPanel from '@mui/lab/TabPanel';
import * as React from "react";
import TableBody from "@mui/material/TableBody";
import {updateProgram} from "../../store/actions/programActions";
import {Button} from "react-bootstrap";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 900,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};
const style2 = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 500,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

const ProgramComponent = (props) => {
    const [viewPrograms, setViewPrograms] = useState(false);
    const [clickedId,setClickedId] = useState(0);
    const [open, setOpen] = useState(false);
    const [open2, setOpen2] = useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () =>{
        setSelectedId(1)
        setEditId(0);
        setAddedId([])
        setDeletedId([])
        setValue(1);
        setOpen(false)
    };
    const handleOpen2 = () => setOpen2(true);
    const handleClose2 = () =>{
        setOpen2(false);
        setSelectedId(1)
        setEditId(0);
        setAddedId([])
        setDeletedId([])
        setValue(1);
    }
    const [value, setValue] = useState(1);
    const [filter, setFilter] = useState('none');
    const [selectedId, setSelectedId] = useState(1)
    const[editId,setEditId] = useState(0);
    const[addedId, setAddedId] = useState([])
    const[deletedId,setDeletedId] = useState([])
    const dispatch = useDispatch();

    const saveChanges = () => {
        let program = {}
        for (let i = 0; i < props.programs.length; i++) {
            if(editId == props.programs[i].id){
                program = props.programs[i]
            }
        }
        let workouts = program.workouts;
        let workoutObjects = [];
        for (let i = 0; i < props.workouts.length; i++) {
            for (let j = 0; j < workouts.length; j++) {
                if(workouts[j] == props.workouts[i].id){
                    let add = true;
                    for (let k = 0; k < deletedId.length; k++) {
                        if(deletedId[k]==workouts[j]) add = false;
                    }
                    if (add)workoutObjects.push(workouts[j]);
                }
            }
        }

        let id_s = workoutObjects.concat(addedId);
        const updateProgramPayload = {"workoutIds":id_s,"programId":editId}
        dispatch(updateProgram(updateProgramPayload));
        handleClose2()
    }

    const editProgramPromt = () => {
        if (editId===0) return;
        let program = {}
        for (let i = 0; i < props.programs.length; i++) {
            if(editId == props.programs[i].id){
                program = props.programs[i]
            }
        }

        let workouts = program.workouts;
        let workoutObjects = [];

        for (let i = 0; i < props.workouts.length; i++) {
            for (let j = 0; j < workouts.length; j++) {
                if(workouts[j] == props.workouts[i].id){
                    let add = true;
                    for (let k = 0; k < deletedId.length; k++) {
                        if(deletedId[k]==workouts[j]) add = false;
                    }
                    if (add)workoutObjects.push(props.workouts[i]);
                }
            }
        }

        let addedWorkoutObject = []
        for (let i = 0; i < props.workouts.length; i++) {
            for (let j = 0; j < addedId.length; j++) {
                if(addedId[j] == props.workouts[i].id){
                    addedWorkoutObject.push(props.workouts[i]);
                }
            }
        }
        let workoutsMadeByContributor = JSON.parse(JSON.stringify(props.workouts))
        workoutsMadeByContributor = workoutsMadeByContributor.filter((workout) => workout.madeByContributor === true);

        return(
            <>
                <h2>{program.title}</h2>
                <TableContainer sx={{ maxWidth: 400 }}component={Paper}>
                    <Table  aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell >Workout name</TableCell>
                                <TableCell >Workout types</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody >
                            {workoutObjects.map((row) => (
                                <TableRow key={row.id}>
                                    <TableCell component="th" scope="row">
                                        {row.title}
                                    </TableCell>
                                    <TableCell>{row.type}</TableCell>
                                    <TableCell><Button className="cancel-button" onClick={()=>setDeletedId([...deletedId,row.id])}>Delete</Button></TableCell>
                                </TableRow>
                            ))}
                            {addedWorkoutObject.map((row) => (
                                <TableRow key={row.id}>
                                    <TableCell component="th" scope="row">
                                        {row.title}
                                    </TableCell>
                                    <TableCell>{row.type}</TableCell>
                                    <TableCell><Button className="cancel-button" onClick={
                                        ()=>setAddedId(addedWorkoutObject.filter((added) =>
                                            added!==row.id))}>Delete</Button></TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
                <select onChange={e => setSelectedId(e.target.value)} name="" id="">
                    {workoutsMadeByContributor.map((workout)=> <option value={workout.id}>{workout.title}</option>)}
                </select>
                <Button className="custom-button" onClick={() => {
                    setAddedId([...addedId, selectedId])
                }}>Add Workout</Button>
                <br/>
                <Button className="custom-button" onClick={()=>saveChanges()}>Save changes</Button>
            </>
        )
    }

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    const onclickViewExercises = () => {
        if(viewPrograms){
            setViewPrograms(false)
        }
        else{
            setViewPrograms(true)
        }
    }
    
    const renderPrograms = () => {
        let programs = []
        if(filter!== "none"){
            programs = JSON.parse(JSON.stringify(props.programs));
            programs = programs.filter((program)=> program.category.includes(filter))
        }
        else{
            programs = props.programs
        }
        return (
            programs.map((field) => (
                <TableHead style={{cursor:'pointer'}} key={field.id}>
                    <TableCell onClick={()=>{setClickedId(field.id); handleOpen()}} > {field.title} </TableCell>
                    <TableCell onClick={()=>{setClickedId(field.id); handleOpen()}} > {field.category} </TableCell>
                    {editButton(field.id)}
                </TableHead>
            ))
        )
    }

    const editButton = (id) =>{
        if(props.auth.contributor === 3 || props.auth.admin === 3){
            return (
                <TableCell onClick={()=>{setEditId(id);handleOpen2()}} ><Button
                    className="custom-button complete-btn ">Edit</Button> </TableCell>
            )
        }
        else return "";
    }

    const renderProgramHead = () =>{
        if(!viewPrograms){
            return(
                <TableHead className="coloredHeader">
                    <TableRow style={{cursor:'pointer'}}onClick={onclickViewExercises}>
                        <TableCell className="headerText" align={"center"}>Click to view programs</TableCell>
                    </TableRow>
                </TableHead>
            )
        }
        else {
            return(
                <>
                    <TableHead className="coloredHeader">
                        <TableRow style={{cursor:'pointer'}} onClick={onclickViewExercises}>
                            <TableCell className="headerText">Program title</TableCell>
                            <TableCell className="headerText">Program category</TableCell>
                            <TableCell></TableCell>
                        </TableRow>
                    </TableHead>
                    {renderPrograms()}
                </>
            )
        }
    }

    const sets = (displayWorkoutId) => {
        let setId = []
        for (const workout of props.workouts) {
            if(workout.id===displayWorkoutId){
                for(const id of workout.sets){
                    setId.push(id);
                }
            }
        }
        let row = []
        for(const id of setId){
            for (const set of props.sets){
                if(id === set.id){
                    row.push({exercise: set.exercise.name, sets: set.sets, reps: set.reps})
                }
            }
        }
        return(
            <TableBody >
                {row.map((row) => (
                    <TableRow key={row.id}>
                        <TableCell component="th" scope="row">
                            {row.exercise}
                        </TableCell>
                        <TableCell>{row.sets}</TableCell>
                        <TableCell>{row.reps}</TableCell>
                    </TableRow>
                ))}
            </TableBody>
        )
    }


    const viewProgramInfo = (selected) => {
        if(clickedId!== 0){
            let day = 1;
            return (selected.workouts.map((field) => (
                <TabPanel value={day++} >
                    <TableContainer component={Paper}>
                        <Table sx={{ minWidth: 300 }} wrapperStyle={{ maxHeight: 100 }}>
                            <TableHead align={"center"}>
                                <TableRow style={{cursor:'pointer'}}  align={"center"} >
                                    <TableCell>Exercise</TableCell>
                                    <TableCell>Reps</TableCell>
                                    <TableCell>Sets</TableCell>
                                </TableRow>
                            </TableHead>
                            {sets(field)}
                        </Table>
                    </TableContainer>
            </TabPanel>
        )))}
    }
    const selectedProgramView = (selected) => {
        let day = 0
        if(clickedId !== 0){return (
        selected.workouts.map((field) => (
                <Tab label={"Workout " + ++day} value={day}/>
        )))}
        return ""
    }

    const programSpecs = () => {
        let selectedProgram = [{}];
        for(const program of props.programs){
            if(program.id === clickedId){
                selectedProgram = program;
            }
        }

        return (
            <>
                <h1 align={"center"}>{selectedProgram.title}</h1>
                <Box sx={{ width: '100%', typography: 'body1' }}>
                    <TabContext value={value}>
                        <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                            <TabList onChange={handleChange} aria-label="lab API tabs example">
                                {selectedProgramView(selectedProgram)}
                            </TabList>
                        </Box>

                        {viewProgramInfo(selectedProgram)}

                    </TabContext>
                </Box>
            </>
        )
    }

    return (
        <div>
            {viewPrograms &&
                <>
                    <b>Filter by Category&nbsp;&nbsp;&nbsp;&nbsp;</b>
                    <select onChange={(e)=>setFilter(e.target.value)}>
                        <option value="none">No Filter</option>
                        <option value="strength">Strength</option>
                        <option value="balance">Balance</option>
                        <option value="flexibility and mobility">Flexibility and Mobility</option>
                        <option value="aerobics">Aerobics</option>
                        <option value="coordination and agility">Coordination and Agility</option>
                        <option value="Legs">Legs</option>
                    </select>
                    <br/><br/>
                </>
            }

            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                    {renderProgramHead()}
                </Table>
            </TableContainer>
            <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    {programSpecs()}
                </Box>
            </Modal>
            <Modal
                open={open2}
                onClose={handleClose2}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style2}>
                    {editProgramPromt()}
                </Box>
            </Modal>
        </div>
    );
}

const mapStateToProps = (state) => {
    return {
        auth: state.auth,
        exercises: state.exercises,
        workouts: state.workouts.workouts,
        sets : state.overview.sets,
        programs: state.programs.programs
    };
}


export default withRouter(connect(mapStateToProps)(ProgramComponent));